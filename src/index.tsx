import { hot } from "react-hot-loader";
import * as React from "react";
import { render } from "react-dom";

var a = 1;

const rootEl = document.getElementById("root");

const App = () => {
  return <h1>Hello World1! {new Date().toISOString()}</h1>;
};

const Re = hot(module)(App)


render(<Re />, rootEl);
